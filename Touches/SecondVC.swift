//  SecondVC.swift
//  Touches
//  Created by Viktoriia Skvarko


import UIKit

class SecondVC: UIViewController {
    
    @IBAction func homeAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBOutlet weak var tapOneOutlet: UIImageView!
    @IBOutlet weak var tapTwoOutlet: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let recognizerTapOne = UITapGestureRecognizer(target: self, action: #selector(tapOne(_:)))
        tapOneOutlet.addGestureRecognizer(recognizerTapOne)
        
        
        let recognizerTapTwo = UITapGestureRecognizer(target: self, action: #selector(tapTwo(_:)))
        tapTwoOutlet.addGestureRecognizer(recognizerTapTwo)
        
    }
    
    @objc func tapOne(_ recognizer: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ThirdVC") as! ThirdVC
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @objc func tapTwo(_ recognizer: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ThirdVC") as! ThirdVC
        self.present(vc, animated: true, completion: nil)
        recognizer.numberOfTapsRequired = 2
    }
    
}
