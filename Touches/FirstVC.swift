//  ViewController.swift
//  Touches
//  Created by Viktoriia Skvarko


import UIKit

class FirstVC: UIViewController {
    
    @IBAction func panBallOneAction(_ recognizer: UIPanGestureRecognizer) {
        
        guard let recognizerView = recognizer.view  else {
            return
        }
        let translation = recognizer.translation(in: view)
        recognizerView.center.x += translation.x
        recognizerView.center.y += translation.y
        recognizer.setTranslation(.zero, in: view)
    }
    
    
    @IBAction func panBallSecondAction(_ recognizer: UIPanGestureRecognizer) {
        
        guard let recognizerView = recognizer.view  else {
            return
        }
        let translation = recognizer.translation(in: view)
        recognizerView.center.x += translation.x
        recognizerView.center.y += translation.y
        recognizer.setTranslation(.zero, in: view)
    }
    
    
    @IBAction func pinchBallOne(_ recognizer: UIPinchGestureRecognizer) {
        if let view = recognizer.view {
            view.transform = view.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
            recognizer.scale = 1
        }
    }
    
    
    @IBAction func pinchBallTwo(_ recognizer: UIPinchGestureRecognizer) {
        if let view = recognizer.view {
            view.transform = view.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
            recognizer.scale = 1
        }
    }
    
    
    @IBAction func rotationBallOne(_ recognizer: UIRotationGestureRecognizer) {
        if let view = recognizer.view {
            view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
        }
    }
    
    
    @IBAction func rotationBallTwo(_ recognizer: UIRotationGestureRecognizer) {
        if let view = recognizer.view {
            view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
        }
    }
    
}

